#pragma once

#include <inttypes.h>
#include <malloc.h>
#include <stddef.h>

#include "dimensions.h"

struct pixel {
  uint8_t components[3];
};

struct image {
  struct dimensions size;
  struct pixel* data;
};

struct image create_image( struct dimensions size );
void delete_image( struct image* image );

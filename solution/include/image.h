#pragma once

#include <stdint.h>

#pragma pack(push, 1)
struct pixel {
  uint8_t r;
  uint8_t g;
  uint8_t b;
};
#pragma pack(pop)

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image create_image(const uint64_t width, const uint64_t height);

void delete_image(struct image* current_image);

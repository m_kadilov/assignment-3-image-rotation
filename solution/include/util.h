#pragma once

#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>

int check_angle(int angle);

FILE* open_file(const char* filename, const char* mode);

void close_file(FILE* file);

struct image read_image_from_file(const char* filename);

enum write_status write_image_to_file(const char* filename, struct image* img);

void set_padding(uint8_t* start, size_t count);

#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>

struct image create_image(const uint64_t width, const uint64_t height) {
    struct image current_image = {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    };

    if (current_image.data == NULL) {
        fprintf(stderr, "Can't allocate memory for image pixels.\n");
        exit(1);
    }

    return current_image;
}

void delete_image(struct image* current_image) {
    if (current_image->data != NULL) {
        free(current_image->data);
    }
}

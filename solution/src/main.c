#include "../include/util.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    // Check arguments
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    int angle = atoi(argv[3]);
    if (!check_angle(angle)) {
        fprintf(stderr, "Invalid angle!\n");
        return 1;
    }

    // Working with image and files
    struct image current_image = read_image_from_file(argv[1]);

    struct image rotated_image = rotate_image(current_image, angle);

    delete_image(&current_image);

    enum write_status status = write_image_to_file(argv[2], &rotated_image);
    if (status == WRITE_ERROR) {
        fprintf(stderr, "ERROR during writing!\n");
        return 1;
    }

    delete_image(&rotated_image);

    printf("SUCCESS\n");

    return 0;
}

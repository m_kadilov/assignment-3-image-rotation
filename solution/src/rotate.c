#include "../include/image.h"
#include "../include/rotate.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


struct image rotate_image_180_degrees(struct image const source) {
    struct image final_image = create_image(source.width, source.height);

    for (uint64_t y = 0; y < source.height; ++y) {
        for (uint64_t x = 0; x < source.width; ++x) {
            final_image.data[(final_image.height - 1 - y) * final_image.width + (final_image.width - 1 - x)] = source.data[y * source.width + x];
        }
    }

    return final_image;
}

struct image rotate_image_90_degrees(struct image const source) {
    struct image final_image = create_image(source.height, source.width);

    for (size_t y = 0; y < source.height; ++y) {
        for (size_t x = 0; x < source.width; ++x) {
            final_image.data[(final_image.height - 1 - x) * final_image.width + y] = source.data[y * source.width + x];
        }
    }

    return final_image;
}

struct image rotate_image_0_degrees(struct image const source) {
    struct image final_image = create_image(source.width, source.height);

    for (uint64_t i = 0; i < source.width * source.height; i++) {
            final_image.data[i] = source.data[i];
    }

    return final_image;
}

struct image rotate_image(struct image const source, int angle) {
    struct image final_image = { 0 };

    if (angle == 90 || angle == -270) {
        final_image = rotate_image_90_degrees(source);
    }
    else if (angle == 180 || angle == -180) {
        final_image = rotate_image_180_degrees(source);
    } 
    else if (angle == -90 || angle == 270) {
        struct image intermediate_image = rotate_image_180_degrees(source);
        final_image = rotate_image_90_degrees(intermediate_image);
        delete_image(&intermediate_image);
    }
    else {                              // Case for invalid or zero rotation angle
        final_image = rotate_image_0_degrees(source);
    }
    
    return final_image;   
}

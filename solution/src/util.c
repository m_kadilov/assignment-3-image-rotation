#include "../include/util.h"
#include "../include/bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void set_padding(uint8_t* start, size_t count) {
    for (size_t i = 0; i < count; ++i) {
        start[i] = 0;
    }
}

int check_angle(int angle) {
    if (angle == 0 || angle == 90 || angle == -90 || angle == 180 || angle == -180 || angle == 270 || angle == -270) {
        return 1;
    } else {
        return 0;
    }
}

FILE* open_file(const char* filename, const char* mode) {
    FILE* file = fopen(filename, mode);

    if (!file) {
        perror("ERROR during file opening!");
    }

    return file;
}

void close_file(FILE* file) {
    if (file) {
        if (fclose(file) != 0) {
            perror("ERROR during file closing!");
        }
    }
}

struct image read_image_from_file(const char* filename) {
    FILE* input_file = open_file(filename, "rb");

    if (!input_file) {
        perror("ERROR during reading image from file!");
        exit(1);
    }

    struct image img;
    enum read_status status = from_bmp(input_file, &img);
    close_file(input_file);

    if (status != READ_OK) {
        perror("ERROR during reading image from file!");
        exit(1);
    }

    return img;
}

enum write_status write_image_to_file(const char* filename, struct image* img) {
    FILE* output_file = open_file(filename, "wb");

    if (!output_file) {
        delete_image(img);
        return WRITE_ERROR;
    }

    enum write_status status = to_bmp(output_file, img);
    close_file(output_file);

    if (status != WRITE_OK) {
        delete_image(img);
        return WRITE_ERROR;
    }

return WRITE_OK;
}

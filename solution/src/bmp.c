#include "../include/util.h"
#include "../include/bmp.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PIXEL_SIZE sizeof(struct pixel)


static const uint16_t BMP_TYPE = 0x4D42; // 'BM' (little endian)

static void copy_pixels(struct pixel* destination, const struct pixel* source, size_t count) {
    for (size_t i = 0; i < count; ++i) {
        destination[i] = source[i];
    }
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = { 0 };

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24 || header.biCompression != 0) {
        return READ_INVALID_BITS;
    }

    *img = create_image(header.biWidth, header.biHeight);
    size_t padding = (4 - (header.biWidth * PIXEL_SIZE) % 4) % 4;
    
    void* row = malloc(header.biWidth * PIXEL_SIZE + padding);
    if (!row) {
        delete_image(img);  
        return READ_INVALID_HEADER;
    }

    for (size_t i = 0; i < header.biHeight; i++) {
        if (fread(row, header.biWidth * PIXEL_SIZE + padding, 1, in) != 1) {
            free(row);
            delete_image(img);
            return READ_INVALID_BITS;
        }
        copy_pixels(img->data + i * header.biWidth, row, header.biWidth);
    }

    free(row);
    
    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* img) {
    size_t padding = (4 - ((uint32_t)img->width * PIXEL_SIZE) % 4) % 4;

    size_t rowSize = img->width * PIXEL_SIZE + padding;
    uint64_t imageSize = rowSize * img->height;
    uint64_t fileSize = sizeof(struct bmp_header) + imageSize;

    if (fileSize > UINT32_MAX || imageSize > UINT32_MAX) {
        fprintf(stderr, "ERROR: Image size more then UINT32_MAX.\n");
        return WRITE_ERROR;
    }

    struct bmp_header header = {
        .bfType = BMP_TYPE,                                 // 'BM' for Bitmap
        .bfileSize = (uint32_t)fileSize,                    // File size (bytes)
        .bfReserved = 0,                                    // Reserved, must be set to 0
        .bOffBits = sizeof(struct bmp_header),              // Offset to start of image data
        .biSize = 40,                                       // BITMAPINFOHEADER size (bytes)
        .biWidth = (uint32_t)img->width,          // Image width in pixels
        .biHeight = (uint32_t)img->height,        // Image height in pixels
        .biPlanes = 1,                                      // Number of color planes
        .biBitCount = 24,                                   // Bits per pixel
        .biCompression = 0,                                 // Compression type
        .biSizeImage = (uint32_t)imageSize,                 // Image size (bytes)
        .biXPelsPerMeter = 0,                               // Horizontal resolution
        .biYPelsPerMeter = 0,                               // Vertical resolution
        .biClrUsed = 0,                                     // Colors in the color palette
        .biClrImportant = 0                                 // Important colors
    };


    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    void* row = malloc(rowSize);
    if (!row) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < img->height; i++) {
        copy_pixels(row, img->data + i * img->width, img->width);
        set_padding((uint8_t*)row + img->width * PIXEL_SIZE, padding);

        if (fwrite(row, rowSize, 1, out) != 1) {
            free(row);
            return WRITE_ERROR;
        }
    }

    free(row);

    return WRITE_OK;
}
